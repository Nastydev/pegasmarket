<?php

namespace App\Controller\Admin;

use DateTime;
use App\Entity\SlotTime;
use App\Repository\CallRequestRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{

    private $repository;

    public function __construct(CallRequestRepository $repository)
    {
        $this->repository =  $repository;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        $lastFives = $this->repository->findLastFiveFields();    
        
        $date = new \DateTime();
        $todayRequests = $this->repository->findTodayFields($date);      
              
        return $this->render('admin/dashboard.html.twig', [
            'lastFives' => $lastFives,
            'todayRequests' => $todayRequests,
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Pegasmarket');
    }

    public function configureMenuItems(): iterable
    {
        return [
            yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),

            yield MenuItem::section('Créneaux horraires'),
            yield MenuItem::linkToCrud('Liste', 'fa fa-list', SlotTime::class),
            yield MenuItem::linkToCrud('Nouveau créneau', 'fas fa-plus', SlotTime::class)
            ->setAction('new'),
            
            yield MenuItem::section(''),
            yield MenuItem::linkToRoute('Home Pegase Market', 'fas fa-chevron-circle-left', 'home'),
        ];
        
    }
}