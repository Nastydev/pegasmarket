<?php

namespace App\Controller;

use App\Entity\CallRequest;
use App\Form\CallRequestType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CallRequestController extends AbstractController
{
     /**
     * @Route("/new", name="call_request_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $callRequest = new CallRequest();

        $form = $this->createForm(CallRequestType::class, $callRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($callRequest);
            $entityManager->flush();

            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('call_request/index.html.twig', [
            'call_request' => $callRequest,
            'form' => $form,
        ]);
    }
}