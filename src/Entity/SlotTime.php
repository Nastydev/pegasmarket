<?php

namespace App\Entity;

use App\Repository\SlotTimeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SlotTimeRepository::class)
 */
class SlotTime
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private $slot;

    /**
     * @ORM\OneToMany(targetEntity=CallRequest::class, mappedBy="slot")
     */
    private $callRequests;

    public function __toString() {

        return $this->slot;
    }

    public function __construct()
    {
        $this->callRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlot(): ?string
    {
        return $this->slot;
    }

    public function setSlot(string $slot): self
    {
        $this->slot = $slot;

        return $this;
    }

    /**
     * @return Collection|CallRequest[]
     */
    public function getCallRequests(): Collection
    {
        return $this->callRequests;
    }

    public function addCallRequest(CallRequest $callRequest): self
    {
        if (!$this->callRequests->contains($callRequest)) {
            $this->callRequests[] = $callRequest;
            $callRequest->setSlot($this);
        }

        return $this;
    }

    public function removeCallRequest(CallRequest $callRequest): self
    {
        if ($this->callRequests->removeElement($callRequest)) {
            // set the owning side to null (unless already changed)
            if ($callRequest->getSlot() === $this) {
                $callRequest->setSlot(null);
            }
        }

        return $this;
    }
}