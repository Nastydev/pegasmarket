<?php 

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class IsPhoneNumberValidator extends ConstraintValidator
{

    /**
     *
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {

        if (null === $value || '' === $value) {
            return;
        }

        if (strlen($value) != 10){
            $this->context->buildViolation($constraint->message)
            ->addViolation();
        }

        if (!preg_match('/^[0-9]+$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}