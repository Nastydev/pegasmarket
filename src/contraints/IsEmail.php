<?php

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

 class IsEmail extends Constraint
 {

    public string $message = 'L\'adresse mail n\'est pas valide';

    /**
     *
     * @return string
     */
    public function validatedBy(): string
    {
        return static::class.'Validator';
    }

 }