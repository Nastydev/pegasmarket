<?php 

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class IsNumberValidator extends ConstraintValidator
{

    /**
     *
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {

        if (null === $value || '' === $value) {
            return;
        }

        if (strlen($value) != 1){
            return;
        }

        if (!is_numeric($value)) {
            $this->context->buildViolation($constraint->message)
            ->addViolation();
        }

        if (!preg_match('/^[1-2]+$/', $value)) {
            // the argument must be a string or an object implementing __toString()
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}