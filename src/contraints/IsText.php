<?php

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

 class IsText extends Constraint
 {

    public string $message = 'Certains symboles ne sont pas autorisés';

    /**
     *
     * @return string
     */
    public function validatedBy(): string
    {
        return static::class.'Validator';
    }

 }