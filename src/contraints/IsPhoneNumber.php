<?php

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

 class IsPhoneNumber extends Constraint
 {

    public string $message = 'Le numéro de téléphone n\'est pas au bon format';

    /**
     *
     * @return string
     */
    public function validatedBy(): string
    {
        return static::class.'Validator';
    }

 }