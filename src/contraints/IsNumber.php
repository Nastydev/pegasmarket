<?php

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

 class IsNumber extends Constraint
 {

    public string $message = 'Ce créneau horaire n\'est pas correct';

    /**
     *
     * @return string
     */
    public function validatedBy(): string
    {
        return static::class.'Validator';
    }

 }