# Pegase Market 

## Environnement

### Prerequisite 

* PHP 7.3.21
* Composer
* nodejs

## Installation 

### Une fois le clone du projet

* Executer la commande `composer install` dans le terminal.
* Executer la commande `npm install` dans le terminal.
* Configuration de la base de donnée: **.env.local** (copié et collé le fichier .env & le renomer **.env copy**).
* Executer la commande `symfony console doctrine:migrations:migrate` dans le terminal.
* Importer la base de données pegasemarket.sql